#ifndef LIGHTRENDER_H
#define LIGHTRENDER_H

#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>
#include <QOpenGLExtraFunctions>
#define PI 3.14159265f
class LightRender
{
public:
    LightRender() = default;
    LightRender(const LightRender&) = delete;
    void initsize(float r);
    void render(QOpenGLExtraFunctions *f, QMatrix4x4 &pMatrix, QMatrix4x4 &vMatrix, QMatrix4x4 &mMatrix, QVector3D &camera, QVector3D &lightLocation);

private:
    QOpenGLShaderProgram program_;
    QOpenGLBuffer vbo_;
    QVector<GLfloat> points_;
    float r_;
};

#endif // LIGHTRENDER_H
