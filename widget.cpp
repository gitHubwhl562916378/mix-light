#include <QWheelEvent>
#include "widget.h"

Widget::Widget(QWidget *parent)
    : QOpenGLWidget(parent),
      eye_(0.0,0.0,2.0),
      center_(0.0,0.0,-2.0),
      lightLocation_(1.0,1.5,1)
{
}

Widget::~Widget()
{

}

void Widget::initializeGL()
{
    render_.initsize(0.8);
}

void Widget::paintGL()
{
    QOpenGLExtraFunctions *f = QOpenGLContext::currentContext()->extraFunctions();
    f->glClearColor(0.0, 0.0, 0.0, 0.0);
    f->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    QMatrix4x4 view;
    view.lookAt(eye_,center_,QVector3D{0.0,1.0,2.0});

    mMatrix.rotate(30,0,1,0);
    render_.render(f,pMatrix,view,mMatrix,eye_,lightLocation_);
}

void Widget::resizeGL(int w, int h)
{
    pMatrix.setToIdentity();
    pMatrix.perspective(45,float(w)/h,0.01f,100.0f);
}

void Widget::wheelEvent(QWheelEvent *event)
{
    if (! event->pixelDelta().isNull()) {
        eye_.setZ(eye_.z() + event->pixelDelta().y()); //重置视点z值
    } else if (!event->angleDelta().isNull()) {
        eye_.setZ(eye_.z() + (event->angleDelta() / 120).y()); //重置视点z值
    }

    update();
    event->accept();
}
