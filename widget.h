#ifndef WIDGET_H
#define WIDGET_H

#include <QOpenGLWidget>
#include "lightrender.h"
class Widget : public QOpenGLWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = 0);
    ~Widget();

protected:
    void initializeGL() override;
    void paintGL() override;
    void resizeGL(int w, int h) override;
    void wheelEvent(QWheelEvent *event) override;

private:
    LightRender render_;
    QVector3D eye_,center_,lightLocation_;
    QMatrix4x4 pMatrix,mMatrix;
};

#endif // WIDGET_H
